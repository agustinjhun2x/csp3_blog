<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Authentication routes*/
// Route::get('auth/login', 'Auth\AuthController@getLogin');
// Route::post('auth/login', 'Auth\AuthController@postLogin');
// Route::get('auth/logout', 'Auth\AuthController@getLogout');

/* Registration Routes */
// Route::get('auth/register', 'Auth\AuthController@getRegister');
// Route::post('auth/register', 'Auth\AuthController@postRegister');


/* Route to blog specific post*/
Route::get('blog/{slug}',['as'=>'blog.single','uses' => 'BlogController@getSingle'])->where('slug','[\w\d\-\_]+');

/* Route to all list of blog posts*/
Route::get('blog',['uses' => 'BlogController@getIndex', 'as' => 'blog.index']);

/* About route*/
Route::get('about', 'PagesController@getAbout');

/* Contact route*/
Route::get('contact', 'PagesController@getContact');
Route::post('contact', 'PagesController@postContact');

/* Subscribe Route*/
Route::get('subscribe', 'PagesController@getSub');
Route::post('subscribe', 'PagesController@postSub');

Route::get('/', 'PagesController@getIndex');

/* Tell laravel that we are managing resource*/
Route::resource('posts','PostsController');

/* Route to categories*/
Route::resource('categories','CategoryController',['except'=>['create']]);
Route::resource('tags','TagController',['except'=>['create']]);

/* Comments route */
Route::post('comments/{post_id}', ['uses'=>'CommentsController@store', 'as' => 'comments.store']);
Route::get('comments/{id}/edit',['uses'=>'CommentsController@edit', 'as' => 'comments.edit']);
Route::put('comments/{id}',['uses'=>'CommentsController@update', 'as' => 'comments.update']);
Route::delete('comments/{id}',['uses'=>'CommentsController@destroy', 'as' => 'comments.destroy']);
Route::get('comments/{id}/delete',['uses'=>'CommentsController@delete', 'as' => 'comments.delete']);


Route::get('profile', 'HomeController@profile');




/*Home page route showing 4 of oldest articles (to edit as 4 featured articles when I update db*/
Route::get('/home', 'ArticlesController@showFeaturedArticles');


/*Articles List route display all articles in paginate*/
Route::get('/articles', 'ArticlesController@showArticles');






/*Just to check if layout works*/
Route::get('/layout', function () {
    return view('applayout');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
