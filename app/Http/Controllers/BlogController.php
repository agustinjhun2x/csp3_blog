<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\User;
use App\Category;

class BlogController extends Controller
{

	public function getIndex() {
		$posts = Article::orderBy('id','desc')->paginate(5);
        $categories = Category::all();
		return view('blog.index')->withPosts($posts)->withCategories($categories);
	}


    public function getSingle($slug) {

    	//get data from DB based on slug
    	$post = Article::where('slug','=',$slug)->first();  /*first() mean take only the first of  the object*/

    	// return the view
    	return view('blog.single')->withPost($post);
    }


}
