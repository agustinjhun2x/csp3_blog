<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article; /*name of the table article from my database*/
use App\Category;
use App\Tag;
use App\User;
use Session;
use Auth;


class PostsController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        // return view('posts.profile')->with('articles', $user->article);

        //create variable and store the blog posts
        // $posts = Article::orderBy('id','desc')->paginate(15);

        $categories = Category::all();

        //return a view and pass the variable above
        return view('posts.index')->withPosts($user->article)->withCategories($categories);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.create')->withCategories($categories)->withTags($tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // data validation (server side)
        $this->validate($request,array(
            'title'         => 'required|max:255',
            'slug'          => 'required|alpha_dash|min:5|max:255|unique:articles,slug',
            'category_id'   => 'required|integer',
            'content'       => 'required'
        ));

        // store in the database
        $post = new Article;
        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->category_id = $request->category_id;
        $post->content = $request->content;

        $uploadimage = $request->file('image');
        $uploadimage->move('img', $uploadimage->getClientOriginalName());

        $post->image = $uploadimage->getClientOriginalName();

        $post->user_id = Auth::user()->id;

        $post->save();

        $post->tags()->sync($request->tags,false);

        //
        Session::flash('success','Post was successfully created!');

        // redirect to another page
        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Article::find($id);
        return view('posts.show')->with('post',$post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //find the specicific post in the db and save as var
        $post = Article::find($id);
        $categories = Category::all();

        $cats = array();
        foreach($categories as $category) {
            $cats[$category->id] = $category->name;
        }

        $tags = Tag::all();
        $tags2 = array();
        foreach ($tags as $tag) {
            $tags2[$tag->id] = $tag->name;
        }

        //return the view
        return view('posts.edit')->withPost($post)->withCategories($cats)->withTags($tags2);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $post = Article::find($id);

        $uploadimage = $request->file('image');

        if($uploadimage == null) {

            if ($request->input('slug') == $post->slug) {
                $this->validate($request,array(
                'title'=>'required|max:255',
                'category_id'=>'required|integer',
                'content'=> 'required'
            ));
            } else {
                $this->validate($request,array(
                'title'=>'required|max:255',
                'slug'=>'required|alpha-dash|min:5|max:255|unique:articles,slug',
                'category_id'=>'required|integer',
                'content'=> 'required'
            ));
            }

            $post->title = $request->input('title');
            $post->slug = $request->input('slug');
            $post->category_id = $request->input('category_id');
            $post->content = $request->input('content');
            $post->save();
            $post->tags()->sync($request->tags);
        
        } else {
            $uploadimage->move('img', $uploadimage->getClientOriginalName());
            $post->image = $uploadimage->getClientOriginalName();
            $post->save();
            $post->tags()->sync($request->tags);

        }


        //set flash data w success message
        Session::flash('success','This Post was successfully updated!');
        //redirect with flashdata post
        return redirect()->route('posts.show',$post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Article::find($id);
        $post->tags()->detach();
        $post->delete();

        Session::flash('success','The post was successfully deleted!');
        return redirect()->route('posts.index');

    }
}
