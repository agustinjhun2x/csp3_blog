<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticlesController extends Controller
{

	//function to show 4 featured articles in home page
    function showFeaturedArticles() {
    	$featured_articles = \App\Article::orderBy('created_at','desc')->take(4)->get();
    	return view('articles.home',compact('featured_articles'));
    }

    //function to show all existing rows in article table
    function showArticles() {
    	$all_articles = \App\Article::paginate(4);
    	return view('articles.articles_list',compact('all_articles'));
    }
}
