<?php

/* Controller that controls all Static Pages */

/* like little containers. you belong to this folder */
namespace App\Http\Controllers; 

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Article;
use App\Category;
use Mail;
use Session;

class PagesController extends Controller {

	public function getIndex() {
		$posts = Article::orderBy('created_at','desc')->limit(4)->get();
        $categories = Category::all();
		return view('pages.welcome')->with('posts',$posts)->withCategories($categories);;
	}	

	public function getAbout() {
		$first = 'Pearl';
		$last = 'Drv';
		$full = $first . " " . $last;
		$email = "contact@pearldrv.com";

		$data = [];
		$data['email'] = $email;
		$data['fullname'] = $full;

		return view('pages.about')->withData($data);
	}

	public function getContact() {
		return view('pages.contact');
	}

	public function postContact(Request $request) {
		$this->validate($request, array(
			'name' => 'required|max:255',
			'email' => 'required|email',
			'subject' => 'min:3',
			'message' => 'min:10'
		));

		$data = array(
			'name' => $request->name,
			'email' => $request->email,
			'subject' => $request->subject,
			'bodyMessage' => $request->message
		);

		Mail::send('emails.contact', $data, function($message) use ($data) {
			$message->from($data['email']);
			$message->to('jhun.m.agustin@gmail.com');
			$message->subject($data['subject']);
		});

		Session::flash('success', 'Your Email was sent!');
		return redirect('contact');
	}

	public function getSub() {
		return view('partials._footer');
	}

	public function postSub(Request $request) {
		$this->validate($request, array(
			'email' => 'required|email'
		));

		$data = array(
			'email' => $request->email,
		);

		Mail::send('emails.subscribe', $data, function($message) use ($data) {
			$message->from($data['email']);
			$message->to('jhun.m.agustin@gmail.com');
		});

		Session::flash('success', 'We will get in touch with you soon!');
		return redirect('/');
	}


}