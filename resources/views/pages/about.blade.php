@extends('applayout')

@section('title', '| About')

@section('banner')
    <!-- Page Header -->
    <header class="masthead" style="background-image: url('img/about.jpeg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="page-heading">
              <h1>About {{ $data['fullname'] }}</h1>
              <span class="subheading">Lorem ipsum dolor sit amet...</span>
            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')
    <!-- Main Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe nostrum ullam eveniet pariatur voluptates odit, fuga atque ea nobis sit soluta odio, adipisci quas excepturi maxime quae totam ducimus consectetur?</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius praesentium recusandae illo eaque architecto error, repellendus iusto reprehenderit, doloribus, minus sunt. Numquam at quae voluptatum in officia voluptas voluptatibus, minus!</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut consequuntur magnam, excepturi aliquid ex itaque esse est vero natus quae optio aperiam soluta voluptatibus corporis atque iste neque sit tempora!</p>
          <p>You can email us at <a href="">{{ $data['email'] }}</a></p>
          <p>Or you can submit your email below. :)</a></p>
        </div>
      </div>
    </div>

@endsection