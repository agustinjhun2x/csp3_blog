@extends('applayout')

@section('title', '| Home')

@section('banner')
    <header class="masthead" style="background-image: url('img/home2-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              @include('partials._messages')
              <h1>Welcome to Pearl Drv!</h1>
              <span class="subheading">Share the best photo that describes your day. Tell a story. Blog it, share it.</span>
        @if (Auth::check())
              <a href="/blog" class="btn btn-primary" style="margin-top: 15px;">Check What Others Have Posted</a>
        @else
              <a href="/register" class="btn btn-primary" style="margin-top: 15px;">Sign Up Now</a>

        @endif

            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')

    <!-- CATEGORIES -->
  <div class="categories">
    <ul class="list-inline">
  @foreach($categories as $category)
      <li class="list-inline-item category-style"><a href="{{ $category->id }}">{{ $category->name}}</a></li>
  @endforeach
    </ul>
  </div>


    <!-- Main Content -->
  <main class="background-container">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
@foreach($posts as $post)
          <div class="post-preview">
            <a href="{{ url('blog/'.$post->slug) }}">
              <h2 class="post-title">
                {{ $post->title }}
              </h2>
              <h3 class="post-subtitle">
                  {{ substr($post->content, 0, 50) }}
                  {{ strlen($post->content) > 50 ? "..." : "" }}
              </h3>
              <div class="featured-image" style="background-image: url(img/{{ $post->image }});"></div>
            </a>
            <p class="post-meta">Posted by
              <a href="#">{{$post->user->name}}</a> on 
              <span  title="
              <?php
                $date = strtotime($post->created_at); /*to parse a string date into a unix timestamp*/
                echo date('j F Y h:i A e', $date); /*PHP date function format*/
           
              ?>">{{ $post->created_at->diffForHumans() }}</span>
            </p>
          </div>
          <hr>
@endforeach
          <!-- Pager -->
          <div class="clearfix">
            <a class="btn btn-primary float-right" href="/blog">More Posts &#8618;</a>
          </div>
        </div>
      </div>
    </div>
  </main>

  @endsection
