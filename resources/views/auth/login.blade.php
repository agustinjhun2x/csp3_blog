@extends('applayout')

@section('title', '| Login')

@section('banner')
    <header class="masthead" style="background-image: url('img/home2-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              <div class="wrapper">
                    <form class="form-signin" method="POST" action="{{ route('login') }}">
                    <h2 class="form-signin-heading">Please Login</h2>
                    <div class="divider"></div>
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <!-- <label for="email" class="control-label">E-Mail Address</label> -->

                  
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong><small>{{ $errors->first('email') }}</small></strong>
                                    </span>
                                @endif
                  
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <!-- <label for="password" class="control-label">Password</label> -->

                                <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong><small>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        
                        </div>

                        <div class="form-group">
                    
                                <div class="checkbox">
                                    <label>
                                        <h6><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me </h6>
                                    </label>
                                </div>
                
                        </div>

                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Login
                                </button>
                                <div class="divider"></div>
                                <h6><a class="" href="{{ route('password.request') }}">
                                    Lost Your Password?
                                </a></h6>
                                <h6>No account yet? Register <a href="/register">here</a></h6>
                            </div>
                        </div>
                    </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

@endsection
