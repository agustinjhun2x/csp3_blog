@extends('applayout')

<?php $titleTag = htmlspecialchars($post->title); ?>

@section('title', "| $titleTag")

@section('banner')
    <header class="masthead" style="background-image: url(../img/{{ $post->image }})">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              @include('partials._messages')
              <h1>{{ $post->title }}</h1>
              <div class="divider"></div>
              <span class="subheading">Posted on {{ date('M j, Y h:ia',strtotime($post->created_at)) }}</span>
              <span class="subheading">Posted in <a href="" class="label label-success">{{ $post->category->name }}</a></span>
            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')
    <!-- Post Content -->
    <article>

      <div class="container">
        <div class="row text-center margin-t">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="categories">
<!--              <p class="site-heading"><small>Tags: </small>
            @foreach($post->tags as $tag)
                <span class="label label-primary">{{ $tag->name }}</span>
            @endforeach
            </p> -->
            </div>


          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <p class="blog-content">{{ $post->content }}</p>
          </div>
        </div>
      </div>
    </article>

    <hr>

  <div class="container-fluid">
    <div class="desc">
      <h4>Comments <small>( {{ $post->comments()->count()}} )</small></h4>
      <div class="divider"></div>
    </div>

@foreach($post->comments as $comment)
<div class="container">
    <div class="row text-center">
        <div class="col-lg-8 col-md-10 mx-auto">
            <div class="panel panel-white post">
                <div class="post-heading">
                    <div class="pull-left image">
                        <img src="{{
                         'https://www.gravatar.com/avatar/' . md5(strtolower(trim($comment->email))) . '?d=monsterid'

                       }}" 


                        class="img-circle avatar" alt="user profile image">
                    </div>
                    <div class="pull-left meta">
                        <div class="title h5">
                            <small><a href="#">{{ $comment->name }}</a>
                            posted a comment:</small>
                        </div>
                        <h6 class="text-muted time"><small>{{ $comment->created_at->diffForHumans() }}</small></h6>
                    </div>
                </div> 
                <div class="post-description"> 
                    <p>{{ $comment->comment }}</p>
                    <div class="stats">
                        <a href="#" class="btn btn-default stat-item">
                            <i class="fa fa-thumbs-up icon"></i>
                        </a>
                        <a href="#" class="btn btn-default stat-item">
                            <i class="fa fa-thumbs-down icon"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="divider"></div>
@endforeach

    <div class="desc">
      <h4>You can post your comments here too</h4>
      <div class="divider"></div>

      {{ Form::open(['route' => ['comments.store', $post->id], 'method' => 'POST']) }}

      <div class="form-group floating-label-form-group controls">
        {{ Form::label('name','Name',['class' => 'small']) }}
        {{ Form::text('name', null, ['class' => 'form-control textboxes','placeholder' => 'Name', 'required data-validation-required-message' => 'Please enter your name.']) }}
        <p class="help-block text-danger"></p>
      </div>

      <div class="form-group floating-label-form-group controls">
        {{ Form::label('email','Email',['class' => 'small']) }}
        {{ Form::text('email', null, ['class' => 'form-control textboxes','placeholder' => 'Email', 'required data-validation-required-message' => 'Please enter your email.']) }}
        <p class="help-block text-danger"></p>
      </div>

      <div class="form-group floating-label-form-group controls">
        {{ Form::label('comment','Comment',['class' => 'small']) }}
        {{ Form::textarea('comment', null, ['class' => 'form-control textboxes','placeholder' => 'Comment', 'required data-validation-required-message' => 'Please enter your comment.']) }}
        <p class="help-block text-danger"></p>
      </div>

      {{ Form::submit('Add Comment', ['class' => 'btn btn-primary', 'style' => 'margin-top: 15px']) }}
        
      {{ Form::close() }}
    </div>
    
  </div>


 @endsection
