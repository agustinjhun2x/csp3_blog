@extends('applayout')

@section('title', '| Blog')

@section('banner')
    <header class="masthead" style="background-image: url('../img/blog2.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading small-banner">
              <h1>Blog Feed</h1>
              <span class="subheading">You can find all the Blogs from other bloggers here.</span>
            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')

    <!-- CATEGORIES -->
  <div class="categories">
    <ul class="list-inline">
  @foreach($categories as $category)
      <li class="list-inline-item category-style"><a href="{{ $category->id }}">{{ $category->name}}</a></li>
  @endforeach
    </ul>
  </div>


    <!-- Main Content -->
  <main class="background-container">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
@foreach($posts as $article)
          <div class="post-preview">
            <a href="{{ route('blog.single', $article->slug) }}">
              <h2 class="post-title">
                {{ $article->title }}
              </h2>
              <h3 class="post-subtitle">
                {{ substr($article->content,0,250) }}
                {{ strlen($article->content) > 250 ? '...' : "" }}
              </h3>
              <div class="featured-image" style="background-image: url(../img/{{ $article->image }});"></div>
            </a>
            <p class="post-meta">Published by
              <a href="#">{{ $article->user->name }}</a>
               
              {{ $article->created_at->diffForHumans() }} |
            @foreach($article->tags as $tag)
                <span class="label label-theme">{{ $tag->name }}</span>
            @endforeach
          </p>
          </div>
          <hr>
@endforeach


          <!-- Pager -->
          <div class="clearfix">
          	{{ $posts->links() }}
          </div>
        </div>
      </div>
    </div>
  </main>


  @endsection