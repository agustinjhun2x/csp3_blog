<!DOCTYPE html>
<html lang="en">

<head>
  @include('partials._head')
</head>

  <body>

    @include('partials._nav')

      <!-- Page Header Goes Here -->
      @yield("banner")

      
      
      <!-- Main Content Goes Here -->
      @yield("main")

<!--     <hr> -->
    
    @include('partials._footer')

    <!-- Bootstrap core JavaScript -->
    <script src='{{ url("vendor/jquery/jquery.min.js") }}'></script>
    <script src='{{ url("vendor/bootstrap/js/bootstrap.bundle.min.js") }}'></script>

    <!-- Custom scripts for this template -->
    <script src='{{ url("js/clean-blog.min.js") }}'></script>
    <!-- SELECT script -->
     {!! Html::script('js/select2.min.js') !!}


    <script type="text/javascript">
      $(document).ready(function(){
          $('body').append('<div id="toTop" class="btn btn-primary"><i class="fa fa-arrow-up fa-inverse" aria-hidden="true"></i></div>');
          $(window).scroll(function () {
          if ($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
          } else {
            $('#toTop').fadeOut();
          }
        }); 
        $('#toTop').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });
    });
    </script>

    <script type="text/javascript">
      $('.select2-multi').select2();
    </script>


  </body>

</html>
