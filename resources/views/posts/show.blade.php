@extends('applayout')

@section('title', '| Post')

@section('banner')
    <header class="masthead" style="background-image: url(../img/{{ $post->image }})">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              @include('partials._messages')
              <h1>{{ $post->title }}</h1>
              <div class="divider"></div>
              <span class="subheading">Posted on {{ date('M j, Y h:ia',strtotime($post->created_at)) }}</span>
              <span class="subheading">Posted in <a href="" class="label label-success">{{ $post->category->name }}</a></span>

            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')
    <!-- Post Content -->
    <article>

      <div class="container">
        <div class="row text-center margin-t">
          <div class="col-lg-8 col-md-10 mx-auto">
            {!! Html::linkRoute('posts.edit','Edit',array($post->id),array('class' => 'btn btn-raised btn-info')) !!}
             <!-- <button type="button" class="btn btn-raised btn-info">Edit</button> -->

             {!! Form::open(['route' => ['posts.destroy',$post->id], 'method' => 'DELETE', 'style' => 'display: inline-block']) !!}

            {!! Form::submit('Delete',['class' => 'btn btn-raised btn-danger','style' => 'cursor: pointer' ]) !!}

            {!! Form::close() !!}
            <div class="divider"></div>
            {!! Html::linkRoute('posts.index','<< See All Posts',[],array('class' => 'btn btn-raised btn-success')) !!}

             <p><small>Check how your post would look like: <a href="{{ url('blog/'.$post->slug) }}" target="_blank">{{ url('blog/'.$post->slug) }}</a></small></p>

             <p><small>Last Updated: {{ date('M j, Y h:ia',strtotime($post->updated_at )) }}</small></p>
             
             <p class="site-heading"><small>Tags: </small>
            @foreach($post->tags as $tag)
                <span class="label label-primary">{{ $tag->name }}</span>
            @endforeach
            </p>


             <div class="divider"></div>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <p class="blog-content">{{ $post->content }}</p>
          </div>
        </div>
      </div>
    </article>

  <main class="background-container">

    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <h3 class="text-center margin-t">Manage All Post's Comments</h3>
          <div class="divider"></div>
        <div class="table-responsive">
          <table id="box-table-a" summary="Comments Listing">
              <thead>
                <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Email</th>
                      <th scope="col">Comment</th>
                      <th scope="col">Comment Date</th>
                      <th scope="col"></th>
                  </tr>
              </thead>
              <tbody>

                @foreach($post->comments as $comment)
                  <tr>
                    <td>
                      {{ $comment->name }}
                    </td>
                    <td>
                      {{ $comment->email }}
                    </td>
                    <td>{{ $comment->comment }}</td>
                    <td>{{ date('M j, Y', strtotime($comment->created_at)) }}</td>
                    <td>
                      <a href="{{ route('comments.edit', $comment->id)}}" c>
                        <span class="fa-stack fa-lg">
                        <i class="fa fa-square fa-stack-2x"></i>
                        <i class="fa fa-pencil-square-o fa-stack-1x fa-inverse" aria-hidden="true"></i>
                        </span>
                      </a>
                      <a href="{{ route('comments.delete',$comment->id) }}" class="">
                        <span class="fa-stack fa-lg">
                        <i class="fa fa-square fa-stack-2x"></i>
                        <i class="fa fa-trash-o fa-stack-1x fa-inverse" aria-hidden="true"></i>
                        </span>
                      </a>
                    </td>
                  </tr>


                @endforeach
              </tbody>
          </table>
          </div>
          <!-- Pager -->
          <div class="clearfix">
            
          </div>
        </div>
      </div>
    </div>
  </main>

 @endsection
