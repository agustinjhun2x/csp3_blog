@extends('applayout')

@section('title', '| Create Post')

@section('banner')
    <header class="masthead" style="background-image: url('../img/create-post.jpeg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading small-banner">
              @include('partials._messages')
              <h1>Create New Post</h1>
              <div class="divider"></div>
              <span class="subheading">Share what you wanna share.</span>
            </div>
          </div>
        </div>
      </div>
    </header>
@endsection


@section('main')
    <!-- Post Content -->
    <article>



      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
              <h3 class="text-center margin-t">New Post</h3>
              <div class="divider"></div>
              <div class="form-signin background-container" style="max-width: 100%;">
                  {!! Form::open(array('route' => 'posts.store', 'data-parsley-validate' => '', 'enctype' => 'multipart/form-data')) !!}
                    {{ Form::label('title','Title:',["class" => 'margin-t']) }}
                    {{ Form::text('title',null,array('class' => 'form-control', 'required' => '', 'maxlength' => '255' )) }}

                    {{ Form::label('slug','Slug:') }}
                    {{ Form::text('slug',null,array('class' => 'form-control', 'required' => '', 'minlength' => '5', 'maxlength' => '255' )) }}

                    {{ Form::label('category','Category:') }}
                    <select class="form-control" name="category_id">
                @foreach( $categories as $category)
                      <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
                    </select>

                    {{ Form::label('tags','Tags:') }}
                    <select class="form-control select2-multi" name="tags[]" multiple="multiple">
                @foreach( $tags as $tag)
                      <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                @endforeach
                    </select>


                    {{ Form::label('content',"Content:") }}
                    {{ Form::textarea('content',null,array('class' => 'form-control', 'required' => '' )) }}

                    <input class="margin-t" type="file" name="image">

              </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row text-center margin-t">
          <div class="col-lg-8 col-md-10 mx-auto">
                    {{ Form::submit('Create Post', array('class' => 'btn btn-primary', 'style' => 'margin-top: 15px;')) }}
                  {!! Form::close() !!}
          </div>
        </div>
        <div class="divider"></div>
      </div>


    </article>



 @endsection
