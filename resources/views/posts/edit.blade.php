@extends('applayout')

@section('title', '| Edit Post')

@section('banner')


    <header class="masthead" style="background-image: url(../../{{ $post->image }})">
{!! Form::model($post, ['route' => ['posts.update',$post->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              @include('partials._messages')
              <h1 class="text-center">Edit This Post</h1>
              <div class="divider"></div>
              <span class="subheading">Posted on: {{ date('M j, Y h:ia',strtotime($post->created_at)) }}</span>
            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')
    <!-- Post Content -->
    <article>

      <div class="container">
        <div class="row text-center margin-t">
          <div class="col-lg-8 col-md-10 mx-auto">
            {!! Html::linkRoute('posts.show','Cancel',array($post->id),array('class' => 'btn btn-raised btn-danger')) !!}
             <!-- <button type="button" class="btn btn-raised btn-info">Edit</button> -->
             {{ Form::submit('Save Changes', ['class' => 'btn btn-raised btn-success', 'style' => 'cursor:pointer'] ) }}

             <!-- <button type="button" class="btn btn-raised btn-danger">Delete</button> -->
             <p><small>Last Updated: {{ date('M j, Y h:ia',strtotime($post->updated_at )) }}</small></p>

          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
              <div class="divider"></div>
              <div class="form-signin background-container" style="max-width: 100%;">
            
                    {{ Form::label('title','Title:',['class' => 'margin-t']) }}
                    {{ Form::text('title', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '255' ]) }}

                    {{ Form::label('slug','Slug:',['class' => 'margin-t']) }}
                    {{ Form::text('slug', null, ['class' => 'form-control', 'required' => '', 'minlength' => '5', 'maxlength' => '255' ]) }}

                    {{ Form::label('category_id', 'Category:', ['class' => 'margin-t']) }}
                    {{ Form::select('category_id',$categories,null,['class' => 'form-control'])}}

                    {{ Form::label('tags', 'Tags: ', ['class' => 'margin-t']) }}
                    {{ Form::select('tags[]', $tags, null, ['class' => 'form-control select2-multi', 'multiple' => 'multiple']) }}

                    {{ Form::label('content','Content:',['class' => 'margin-t']) }}
                    {{ Form::textarea('content', null, ['class' => 'form-control', 'required' => '']) }}

                    <input type="file" name="image" class="margin-t" placeholder="Upload image" value="{{ $post->image }}">


              </div>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row text-center margin-t">
          <div class="col-lg-8 col-md-10 mx-auto">
            {!! Html::linkRoute('posts.show','Cancel',array($post->id),array('class' => 'btn btn-raised btn-danger')) !!}
             <!-- <button type="button" class="btn btn-raised btn-info">Edit</button> -->
             {{ Form::submit('Save Changes', ['class' => 'btn btn-raised btn-success', 'style' => 'cursor:pointer'] ) }}

             <!-- <button type="button" class="btn btn-raised btn-danger">Delete</button> -->
      
          </div>
        </div>
        <div class="divider"></div>
      </div>

    </article>

    {!! Form::close() !!}

 @endsection
