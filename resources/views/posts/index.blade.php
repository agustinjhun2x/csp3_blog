@extends('applayout')

@section('title', '| All Posts')

@section('banner')
    <header class="masthead" style="background-image: url('img/blog2.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading small-banner">
              <h1>All Posts</h1>
              <span class="subheading">Manage Your Blog Posts Here.</span>
              <a href="{{ route('posts.create') }}" class="btn btn-primary" style="margin-top: 15px;">Create New Post</a>
            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')

    <!-- CATEGORIES -->
  <div class="categories">
    <ul class="list-inline">
  @foreach($categories as $category)
      <li class="list-inline-item category-style"><a href="{{ $category->id }}">{{ $category->name}}</a></li>
  @endforeach
    </ul>
  </div>




    <!-- Main Content -->
  <main class="background-container">

    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="table-responsive">
          <table id="box-table-a" summary="Blog Posts Listing">
              <thead>
                <tr>
                    <th scope="col">#</th>
                      <th scope="col">Title</th>
                      <th scope="col">Content</th>
                      <th scope="col">Created At</th>
                      <th scope="col"></th>
                  </tr>
              </thead>
              <tbody>

                @foreach(Auth::user()->articles as $post)
                  <tr>
                    <td>{{ $post->id }}</td>
                    <td>
                      {{ substr($post->title, 0, 40) }}
                      {{ strlen($post->title) > 40 ? "..." : "" }}
                    </td>
                    <td>
                      {{ substr($post->content, 0, 40) }}
                      {{ strlen($post->content) > 40 ? "..." : "" }}
                    </td>
                    <td>{{ date('M j, Y', strtotime($post->created_at)) }}</td>
                    <td>
                      <a href="{{ route('posts.show',$post->id) }}" class="btn btn-info small-b">View</a>
                      <a href="{{ route('posts.edit',$post->id)}}" class="btn btn-info small-b">Edit</a>

                    </td>
                  </tr>


                @endforeach
              </tbody>
          </table>
          </div>
          <!-- Pager -->
          <div class="clearfix">
          	
          </div>
        </div>
      </div>
    </div>
  </main>


  @endsection