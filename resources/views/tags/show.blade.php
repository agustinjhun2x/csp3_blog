@extends('applayout')

@section('title', "| $tag->name Tag")

@section('banner')
    <header class="masthead" style="background-image: url(../)">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading small-banner">
              @include('partials._messages')
              <h1>Manage Specific Tag</h1>
              <div class="divider"></div>
              <span class="subheading">All posts related to [ {{ $tag->name }} ] Tag</span>

            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')
    <article>

      <div class="container">
        <div class="row text-center margin-t">
          <div class="col-lg-8 col-md-10 mx-auto">
            
             
             <a href="{{ route('tags.edit',$tag->id) }}" class="btn btn-raised btn-info">Edit Tag</a>

             {!! Form::open(['route' => ['tags.destroy',$tag->id], 'method' => 'DELETE', 'style' => 'display: inline-block']) !!}

            {!! Form::submit('Delete',['class' => 'btn btn-raised btn-danger','style' => 'cursor: pointer' ]) !!}

            {!! Form::close() !!}

             <div class="divider"></div>
              <a href="{{ url('tags')}}" class="btn btn-raised btn-success"><< See All Tags</a>
             <div class="divider"></div>
             <p class="subheading"><h3 class="label label-choice">{{ $tag->name }}</h3> Tag <small>( {{ $tag->articles()->count() }} Posts )</small></p>
          </div>
        </div>
      </div>

    </article>

    <!-- Main Content -->
  <main class="background-container">

    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">

          <table id="box-table-a" summary="tags listing">
              <thead>
                <tr>
                    <th scope="col">#</th>
                      <th scope="col">Post Title</th>
                      <th scope="col">Tags</th>
                      <th scope="col"></th>
                  </tr>
              </thead>
              <tbody>

                @foreach($tag->articles as $post)
                  <tr>
                    <td>{{ $post->id }}</td>
                    <td>
                      {{ substr($post->title, 0, 40) }}
                      {{ strlen($post->title) > 40 ? "..." : "" }}
                    </td>
                    <td>
                    	@foreach($post->tags as $tag1)
                    		<span class="label 
                              <?php if($tag1->id !== $tag->id) { 
                                echo 'label-primary';
                              } else {
                                echo 'label-choice';
                            } ?>">{{ $tag1->name }}</span>
                    	@endforeach
                    </td>
                    <td>
                      <a href="{{ route('posts.show',$post->id) }}" class="btn btn-info small-b">View</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
          </table>

          <!-- Pager -->
          <div class="clearfix">
          	
          </div>
        </div>
      </div>
    </div>
  </main>

 @endsection
