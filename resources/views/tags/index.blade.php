@extends('applayout')


@section('title', '| All Tags')


@section('banner')
    <header class="masthead" style="background-image: url('img/blog2.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading small-banner">
           		@include('partials._messages')
              <h1>All Tags</h1>
              <span class="subheading">Manage Tags Here.</span>
            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')

    <!-- Main Content -->
  <main class="background-container">

    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
       	<h3 class="text-center margin-t">Add New Tag</h3>
        <div class="divider"></div>
            	{!! Form::open(['route' => 'tags.store', 'method' => 'POST', 'class' => 'w-clearfix']) !!}
            		
            		{{ Form::text('name', null, ['class' => 'field w-input', 'placeholder' => 'Type New Tag Here', 'required']) }}
            		{{ Form::submit('Add Tag', ['class' => 'submit-button w-button btn-primary']) }}
            	{!! Form::close() !!}

          <table id="box-table-a" summary="Categories">
              <thead>
                  <tr>
                    <th scope="col"></th>
                    <th scope="col">#</th>
                    <th scope="col">Tag Name</th>
                    <th scope="col">Created At</th>
                  </tr>
              </thead>
              <tbody>

                @foreach($tags as $tag)
                  <tr>
                    <td></td>
                    <td>{{ $tag->id }}</td>
                    <td>
                      <a href="{{ route('tags.show', $tag->id)}}" class="label label-primary">{{ $tag->name }}</a>
                    </td>
                    <td>{{ $tag->created_at }}</td>
                  </tr>

                @endforeach
              </tbody>
          </table>

          <!-- Pager -->
          <div class="clearfix">
          	
          </div>
        </div>
      </div>
    </div>
  </main>


  @endsection