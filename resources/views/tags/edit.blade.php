@extends('applayout')

@section('title', '| Edit This Tag')

@section('banner')
    <header class="masthead" style="background-image: url(../../)">

      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading small-banner">
              @include('partials._messages')
              <h1 class="text-center">Edit This Tag</h1>
              <div class="divider"></div>
              <span class="subheading">{{ $tag->name }}</span>
            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')
    <!-- Post Content -->
    <article>

       <div class="container">
        <div class="row text-center margin-t">
          <div class="col-lg-8 col-md-10 mx-auto">
             <p><small>Last Updated: {{ date('M j, Y h:ia',strtotime($tag->created_at )) }}</small></p>

          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
              <div class="divider"></div>
              <div class="form-signin background-container" style="max-width: 100%;">
           		{!! Form::model($tag, ['route' => ['tags.update',$tag->id], 'method' => 'PUT']) !!}
                    {{ Form::label('name','Tag Name:',['class' => 'margin-t']) }}
                    {{ Form::text('name', null, ['class' => 'form-control', 'required' => '']) }}
  
              </div>
             
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row text-center margin-t">
          <div class="col-lg-8 col-md-10 mx-auto">

            {!! Html::linkRoute('tags.show','Cancel',array($tag->id),array('class' => 'btn btn-raised btn-danger')) !!}

             {{ Form::submit('Save Changes', ['class' => 'btn btn-raised btn-success', 'style' => 'cursor:pointer'] ) }}

	         {!! Form::close() !!}

          </div>
        </div>
        <div class="divider"></div>
      </div>

    </article>



 @endsection
