@extends('applayout')

@section('banner')
    <header class="masthead" style="background-image: url('img/blog2.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              <h1>Blog</h1>
              <span class="subheading">Want to dig more? Check out more of my posts here. Leave a mark when you leave.</span>
            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')

    <!-- CATEGORIES -->
  <div class="categories">
    <ul class="list-inline">
      <li class="list-inline-item category-style"><a href="#">Nature</a></li>
      <li class="list-inline-item category-style"><a href="#">Historic</a></li>
      <li class="list-inline-item category-style"><a href="#">Modern</a></li>
      <li class="list-inline-item category-style"><a href="#">Art</a></li>
      <li class="list-inline-item category-style"><a href="#">Beach</a></li>
    </ul>
  </div>




    <!-- Main Content -->
  <main class="background-container">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
@foreach($all_articles as $article)
          <div class="post-preview">
            <a href='{{ url("articles/$article->id") }}'>
              <h2 class="post-title">
                {{ $article->title }}
              </h2>
              <h3 class="post-subtitle">
                <?php 
                  $content = $article->content; /*save content in a variable*/
                  $dot = "."; /*declare a variable with string .*/
                  $position = stripos($content,$dot); /* finds the position of the first occurrence of a string inside another string.*/

                  if($position) { /*if there is a . in the text*/
                    $offset = $position + 1; /*if there is a ., add 1 to the position*/
                    $position2 = stripos($content, $dot, $offset); /*returns position of the second .*/
                    $first_two = substr($content, 0, ($position2 + 1)); /*save the substring from 0 until the 2nd position of . plus 1*/
                    echo $first_two;
                  } 
                  else {
                    echo "Check this out!";
                  }

                ?>
              </h3>
              <div class="featured-image" style="background-image: url({{ $article->image }});"></div>
            </a>
            <p class="post-meta">Posted by
              <a href="#">Anonymous</a>
              on 
              <?php
              $date = strtotime($article->created_at); /*to parse a string date into a unix timestamp*/
              echo date('F j , Y \a\t h:i A e', $date); /*PHP date function format*/
              ?>  
          </p>
          </div>
          <hr>
@endforeach


          <!-- Pager -->
          <div class="clearfix">
          	{{ $all_articles->links() }}
          </div>
        </div>
      </div>
    </div>
  </main>


  @endsection