@extends('applayout')

@section('title', '| Home')

@section('banner')
    <header class="masthead" style="background-image: url('img/home2-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              <h1>Welcome to Pearl Drv!</h1>
              <span class="subheading">Share the best photo of the place you recently visited. Tell a story. Blog it, share it.</span>
              <a href="signup.html" class="btn btn-primary" style="margin-top: 15px;">Sign Up Now</a>
            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')

    <!-- CATEGORIES -->
  <div class="categories">
    <ul class="list-inline">
      <li class="list-inline-item category-style"><a href="#">Nature</a></li>
      <li class="list-inline-item category-style"><a href="#">Historic</a></li>
      <li class="list-inline-item category-style"><a href="#">Modern</a></li>
      <li class="list-inline-item category-style"><a href="#">Art</a></li>
      <li class="list-inline-item category-style"><a href="#">Beach</a></li>
    </ul>
  </div>


    <!-- Main Content -->
  <main class="background-container">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
@foreach($featured_articles as $featured)
          <div class="post-preview">
            <a href='{{ url("articles/$featured->id") }}'>
              <h2 class="post-title">
                {{ $featured->title }}
              </h2>
              <h3 class="post-subtitle">
                <?php 
                  $content = $featured->content; /*save content in a variable*/
                  $dot = "."; /*declare a variable with string .*/
                  $position = stripos($content,$dot); /* finds the position of the first occurrence of a string inside another string.*/

                  if($position) { /*if there is a . in the text*/
                    $offset = $position + 1; /*if there is a ., add 1 to the position*/
                    $position2 = stripos($content, $dot, $offset); /*returns position of the second .*/
                    $first_two = substr($content, 0, ($position2 + 1)); /*save the substring from 0 until the 2nd position of . plus 1*/
                    echo $first_two;
                  } 
                  else {
                    echo "Check this out!";
                  }

                ?>

              </h3>
              <div class="featured-image" style="background-image: url({{ $featured->image }});"></div>
            </a>
            <p class="post-meta">Posted by
              <a href="#">Anonymous</a> on 
              <span  title="
              <?php
                $date = strtotime($featured->created_at); /*to parse a string date into a unix timestamp*/
                echo date('j F Y h:i A e', $date); /*PHP date function format*/
           
              ?>">{{ $featured->created_at->diffForHumans() }}</span>
            </p>
          </div>
          <hr>
@endforeach
          <!-- Pager -->
          <div class="clearfix">
            <a class="btn btn-primary float-right" href="#">More Posts &#8618;</a>
          </div>
        </div>
      </div>
    </div>
  </main>

  @endsection
