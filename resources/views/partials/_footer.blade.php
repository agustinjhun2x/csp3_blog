<!-- Footer -->
<footer class="dark main-section w-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">

        <h3 class="white text-center">Stay In Touch</h3>
        <div class="divider"></div>
        <div class="form-wrapper">
          <form method="POST" action="{{ url('subscribe') }}" id="email-form" name="email-form" data-name="Email Form" class="w-clearfix">
            {{ csrf_field() }}
            <input type="email" class="field w-input" maxlength="256" name="email" data-name="Email" placeholder="Enter your email address" id="email" required=""><input type="submit" value="Submit" data-wait="Please wait..." class="submit-button w-button">
          </form>

        </div>

        <ul class="list-inline text-center">
          <li class="list-inline-item">
            <a href="#">
              <span class="fa-stack fa-lg">
                <i class="fa fa-square fa-stack-2x"></i>
                <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </li>
          <li class="list-inline-item">
            <a href="#">
              <span class="fa-stack fa-lg">
                <i class="fa fa-square fa-stack-2x"></i>
                <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </li>
          <li class="list-inline-item">
            <a href="#">
              <span class="fa-stack fa-lg">
                <i class="fa fa-square fa-stack-2x"></i>
                <i class="fa fa-github fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </li>
        </ul>
        <p class="copyright text-muted">Copyright &copy; Pearl Drv 2017. All rights reserved.</p>
      </div>
    </div>
  </div>
</footer>