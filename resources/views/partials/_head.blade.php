    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Pearl Drv is a personal blog by Jhun Agustin. It is where I post all my interests in life. That includes sceneries, places, people and many others.">
    <meta name="author" content="">

    <title>Pearl Drv @yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href='{{ url("vendor/bootstrap/css/bootstrap.min.css") }}' rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href='{{ url("vendor/font-awesome/css/font-awesome.min.css") }}' rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">


    <!-- Custom styles for this template -->
    <link href='{{ url("css/style.css") }}' rel="stylesheet">

    <!-- Select2 CSS  -->
    <!-- <link href='path/to/select2.min.css' rel="stylesheet" /> -->
    {!! Html::style('css/select2.min.css') !!}

<!--  <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>

  <script>tinymce.init({ selector:'textarea' });</script>

  -->