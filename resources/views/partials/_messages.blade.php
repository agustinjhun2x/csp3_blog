@if (Session::has('success'))

	<div class="alert alert-success small" role="alert">
		<label for="alertmessage"><strong>Success:</strong> {{ Session::get('success') }}</label>
	</div>	

@endif

@if (count($errors)>0)
	
	<div class="alert alert-danger small" role="alert">
		<label><strong>Errors:</strong> {{ Session::get('success') }}</label>
		<ul class="">
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
		</ul>
	</div>	

@endif