<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
  <div class="container">
    <a class="navbar-brand" href='{{url("/")}}'>PEARL DRV</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      Menu
      <i class="fa fa-bars" id="nav-toggle"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
          <a class="nav-link" href='{{url("/")}}'>Home</a>
        </li>
        <li class="nav-item {{ Request::is('about') ? 'active' : '' }}">
          <a class="nav-link" href='{{url("/about")}}'>About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href='{{url("/blog")}}'>Blog Feed</a>
        </li>
        <li class="nav-item {{ Request::is('contact') ? 'active' : '' }}">
          <a class="nav-link" href='{{url("/contact")}}'>Contact</a>
        </li>

    @if (Auth::check())
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href='/'>{{ Auth::user()->name }}</a>
          <ul class="dropdown-menu">
            <li class="nav-item"><a class="nav-link" href="">Account</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('posts.index')}}">Posts</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('tags.index')}}">Tags</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('categories.index')}}">Categories</a></li>
            <li class="nav-item"><a class="nav-link" href="">Archive</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
          </ul>
        </li>
    @else
        <li class="nav-item">
          <a href="{{ route('login') }}" class="nav-link">Login</a>
        </li>
    @endif

      </ul>
    </div>
  </div>
</nav>