@extends('applayout')

@section('title', '| Edit Comment')

@section('banner')


    <header class="masthead" style="background-image: url(../../)">
{!! Form::model($comment, ['route' => ['comments.update',$comment->id], 'method' => 'PUT']) !!}
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading small-banner">
              @include('partials._messages')
              <h1 class="text-center">Edit Comment</h1>
              <div class="divider"></div>
              <span class="subheading">Posted on: {{ date('M j, Y h:ia',strtotime($comment->created_at)) }}</span>
            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')
    <!-- Post Content -->
    <article>

      <div class="container">
        <div class="row text-center margin-t">
          <div class="col-lg-8 col-md-10 mx-auto">
            {!! Html::linkRoute('posts.show','Cancel',array($comment->post_id),array('class' => 'btn btn-raised btn-danger')) !!}

             {{ Form::submit('Update Comment', ['class' => 'btn btn-raised btn-success', 'style' => 'cursor:pointer'] ) }}

             <p><small>Last Updated: {{ date('M j, Y h:ia',strtotime($comment->updated_at )) }}</small></p>

          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
              <div class="divider"></div>
              <div class="form-signin background-container" style="max-width: 100%;">
            
                    {{ Form::label('name','Name:',['class' => 'margin-t']) }}
                    {{ Form::text('name', null, ['class' => 'form-control', 'disabled' => 'disabled' ]) }}

                    {{ Form::label('email','Email:',['class' => 'margin-t']) }}
                    {{ Form::text('email', null, ['class' => 'form-control', 'disabled' => 'disabled' ]) }}

                    {{ Form::label('comment','Comment:',['class' => 'margin-t']) }}
                    {{ Form::textarea('comment', null, ['class' => 'form-control', 'required' => '']) }}
              </div>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row text-center margin-t">
          <div class="col-lg-8 col-md-10 mx-auto">
            {!! Html::linkRoute('posts.show','Cancel',array($comment->post_id),array('class' => 'btn btn-raised btn-danger')) !!}

             {{ Form::submit('Update Comment', ['class' => 'btn btn-raised btn-success', 'style' => 'cursor:pointer'] ) }}

          </div>
        </div>
        <div class="divider"></div>
      </div>

    </article>

    {!! Form::close() !!}

 @endsection
