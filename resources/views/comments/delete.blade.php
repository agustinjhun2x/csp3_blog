@extends('applayout')

@section('title', '| Delete Comment')

@section('banner')


    <header class="masthead" style="background-image: url(../../)">

      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading small-banner">
              @include('partials._messages')
              <h1 class="text-center">Delete Comment</h1>
              <div class="divider"></div>
            </div>
          </div>
        </div>
      </div>
    </header>
@endsection

@section('main')
    <!-- Post Content -->
    <article>

      <div class="container">
        <div class="row text-center margin-t">
          <div class="col-lg-8 col-md-10 mx-auto">
            <p>Delete this Comment?</p>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
              <div class="divider"></div>
              <div class="form-signin background-container" style="max-width: 100%;">
				<p><strong>Name: </strong>{{ $comment->name }}</p>
				<p><strong>Email: </strong>{{ $comment->email }}</p>
				<p><strong>Comment: </strong>{{ $comment->comment }}</p>

              </div>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row text-center margin-t">
          <div class="col-lg-8 col-md-10 mx-auto">
{!! Form::model($comment, ['route' => ['comments.destroy',$comment->id], 'method' => 'DELETE']) !!}
            {!! Html::linkRoute('posts.show','Cancel',array($comment->post_id),array('class' => 'btn btn-raised btn-info')) !!}


             {{ Form::submit('Delete Comment', ['class' => 'btn btn-raised btn-danger', 'style' => 'cursor:pointer'] ) }}
{!! Form::close() !!}
          </div>
        </div>
        <div class="divider"></div>
      </div>

    </article>



 @endsection
